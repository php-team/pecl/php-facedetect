php-facedetect (1.1.0-29-g0e243c5-5) unstable; urgency=medium

  * Fix the patch to use --cflags to use PHP_EVAL_INCLINE macro

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Dec 2022 13:16:31 +0100

php-facedetect (1.1.0-29-g0e243c5-4) unstable; urgency=medium

  * Use --cflags instead of --variable=includedir from pkg-config

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Dec 2022 12:46:31 +0100

php-facedetect (1.1.0-29-g0e243c5-3) unstable; urgency=medium

  * Regenerate d/control for PHP 8.2

 -- Ondřej Surý <ondrej@debian.org>  Sat, 10 Dec 2022 12:21:40 +0100

php-facedetect (1.1.0-29-g0e243c5-2) unstable; urgency=medium

  * Revert "Temporarily limit the PHP versions to PHP 8.1"
  * Bump the required dh-php to >= 4.9~

 -- Ondřej Surý <ondrej@debian.org>  Thu, 03 Mar 2022 10:38:53 +0100

php-facedetect (1.1.0-29-g0e243c5-1) unstable; urgency=medium

  * New upstream version 1.1.0-29-g0e243c5
  * Temporarily limit the PHP versions to PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Thu, 03 Mar 2022 05:32:29 +0100

php-facedetect (1.1.0-27-g2a8974b-1) unstable; urgency=medium

  * Remove PHP 7 overrides
  * Add upstream support for PHP 8.x
  * New upstream version 1.1.0-27-g2a8974b
  * Add all supported PHP versions and requirement for dh-php >= 4.6
    (Closes: #1000637)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 23 Jan 2022 16:00:34 +0100

php-facedetect (1.1.0-19-g135c72a-10) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:23 +0100

php-facedetect (1.1.0-19-g135c72a-9) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:09:45 +0100

php-facedetect (1.1.0-19-g135c72a-7) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:00:53 +0100

php-facedetect (1.1.0-19-g135c72a-6) unstable; urgency=medium

  * Override the PHP_DEFAULT_VERSION
  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:06:44 +0100

php-facedetect (1.1.0-19-g135c72a-5) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:27 +0100

php-facedetect (1.1.0-19-g135c72a-4) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:50:31 +0100

php-facedetect (1.1.0-19-g135c72a-3) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:10:42 +0100

php-facedetect (1.1.0-19-g135c72a-2) unstable; urgency=medium

  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:39:42 +0100

php-facedetect (1.1.0-19-g135c72a-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add upstream fix for FTBFS with OpenCV 4.5. (Closes: #977248)

 -- Adrian Bunk <bunk@debian.org>  Sun, 07 Feb 2021 09:00:56 +0200

php-facedetect (1.1.0-19-g135c72a-1) unstable; urgency=medium

  * New upstream version 1.1.0-19-g135c72a (used git describe to gen vernum)
    (Closes: #915712, #922590)
  * Modernize the packaging
   - Use dh-php with php-all-dev to build for all available PHP versions
   - Bump standards version
   - Bump debhelper compat level to 12

 -- Ondřej Surý <ondrej@debian.org>  Mon, 02 Mar 2020 09:37:04 +0100

php-facedetect (1.1.0+git20170801-2) unstable; urgency=medium

  * Standards-Version: 4.2.1
  * Update debhelper compat from 9 to 11
  * Package moved to salsa
  * Use secure copyright format URI
  * Update maintainer email to team+php-pecl@tracker.debian.org
    (Closes: #899801)

 -- Mathieu Parent <sathieu@debian.org>  Mon, 05 Nov 2018 13:13:13 +0100

php-facedetect (1.1.0+git20170801-1) unstable; urgency=medium

  * Upload to unstable, OpenCV 3 is coming
  * New upstream snapshot
    - Remove 0001-Fix-configure-script-syntax.patch, merged

 -- Mathieu Parent <sathieu@debian.org>  Fri, 13 Oct 2017 20:59:33 +0200

php-facedetect (1.1.0+git20160406-3) experimental; urgency=medium

  * Upload to experimental, for opencv transition
  * Remove 0002-Revert-opencv3-support.patch: Fixes FTBFS with OpenCV 3.1
    (Closes: #841250)
  * Bump build-depends accordingly

 -- Mathieu Parent <sathieu@debian.org>  Fri, 21 Oct 2016 21:28:48 +0200

php-facedetect (1.1.0+git20160406-2) unstable; urgency=medium

  * Add ${php:Depends} to d/control (Closes: #821824)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 19 Apr 2016 21:22:57 +0200

php-facedetect (1.1.0+git20160406-1) unstable; urgency=medium

  * New upstream release:
    - PHP7 support, patch by me (Closes: #820094)
    - OpenCV 3 support
  * Revert OpenCV3 support until OpenCV 3 is in sid
  * Move from *php5 to *php
  * Lintian:
    - Fix homepage URL
    - Fix Vcs-* fields
    - Standards-Version: 3.9.7
    - enables "PIE" and "BINDNOW" and future hardening flags
  * gbp.conf: rename old style config section [git-buildpackage]
  * 0001-Fix-configure-script-syntax.patch forwarded upstream

 -- Mathieu Parent <sathieu@debian.org>  Thu, 07 Apr 2016 06:57:17 +0200

php-facedetect (1.1.0+git20140717-2) unstable; urgency=medium

  * Don't use versioned provides yet (Closes: #777194)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 06 Feb 2015 09:41:01 +0100

php-facedetect (1.1.0+git20140717-1) unstable; urgency=medium

  * Initial release. (Closes: #775864)

 -- Mathieu Parent <sathieu@debian.org>  Wed, 21 Jan 2015 22:19:09 +0100
